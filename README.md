# boost-your-velocity

Demo Project to support the Boost Your Velocity IT Deep Dive video for Team Rockstars IT

[![pipeline status](https://gitlab.com/phazebroek/boost-your-velocity/badges/master/pipeline.svg)](https://gitlab.com/phazebroek/boost-your-velocity/-/commits/master)

[![coverage report](https://gitlab.com/phazebroek/boost-your-velocity/badges/master/coverage.svg)](https://gitlab.com/phazebroek/boost-your-velocity/-/commits/master)

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=phazebroek_boost-your-velocity&metric=alert_status)](https://sonarcloud.io/dashboard?id=phazebroek_boost-your-velocity)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=phazebroek_boost-your-velocity&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=phazebroek_boost-your-velocity)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=phazebroek_boost-your-velocity&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=phazebroek_boost-your-velocity)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=phazebroek_boost-your-velocity&metric=security_rating)](https://sonarcloud.io/dashboard?id=phazebroek_boost-your-velocity)

[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=phazebroek_boost-your-velocity&metric=ncloc)](https://sonarcloud.io/dashboard?id=phazebroek_boost-your-velocity)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=phazebroek_boost-your-velocity&metric=coverage)](https://sonarcloud.io/dashboard?id=phazebroek_boost-your-velocity)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=phazebroek_boost-your-velocity&metric=bugs)](https://sonarcloud.io/dashboard?id=phazebroek_boost-your-velocity)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=phazebroek_boost-your-velocity&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=phazebroek_boost-your-velocity)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=phazebroek_boost-your-velocity&metric=sqale_index)](https://sonarcloud.io/dashboard?id=phazebroek_boost-your-velocity)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=phazebroek_boost-your-velocity&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=phazebroek_boost-your-velocity)

[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-white.svg)](https://sonarcloud.io/dashboard?id=phazebroek_boost-your-velocity)
