package nl.phazebroek.boostyourvelocity.greeting;

import static nl.phazebroek.boostyourvelocity.TestConstants.GREETING;
import static nl.phazebroek.boostyourvelocity.TestConstants.ID;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class GreetingServiceTest {

  @InjectMocks
  private GreetingService greetingService;

  @Mock
  private GreetingRepository greetingRepository;

  @Test
  void getGreeting() {
    when(greetingRepository.findGreetingById(any())).thenReturn(GREETING);

    var greeting = greetingService.getGreeting(ID);

    then(greeting).isEqualTo(GREETING);
    then(greeting).isEqualTo(GREETING);
    verify(greetingRepository).findGreetingById(ID);
  }
}