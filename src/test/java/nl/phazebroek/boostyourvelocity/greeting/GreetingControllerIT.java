package nl.phazebroek.boostyourvelocity.greeting;

import static nl.phazebroek.boostyourvelocity.TestConstants.GREETING;
import static nl.phazebroek.boostyourvelocity.TestConstants.ID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("integration")
class GreetingControllerIT {

  @Autowired
  protected MockMvc mockMvc;

  @Autowired
  protected ObjectMapper objectMapper;

  @Test
  void getGreetingReturnsGreeting() throws Exception {
    getGreeting()
      .andExpect(status().isOk())
      .andExpect(content().string(GREETING));
  }

  private ResultActions getGreeting() throws Exception {
    return mockMvc.perform(get("/greetings/{id}", ID)
      .contentType(MediaType.APPLICATION_JSON)
    );
  }
}