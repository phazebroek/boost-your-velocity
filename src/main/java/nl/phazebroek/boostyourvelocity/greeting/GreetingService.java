package nl.phazebroek.boostyourvelocity.greeting;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * Service for Greetings.
 */
@Service
@RequiredArgsConstructor
public class GreetingService {

  private final GreetingRepository greetingRepository;

  public String getGreeting(String id) {
    return greetingRepository.findGreetingById(id);
  }
}
