package nl.phazebroek.boostyourvelocity.greeting;

import java.util.Locale;
import org.springframework.stereotype.Component;

/**
 * A dummy repository.
 */
@Component
public class GreetingRepository {

  /**
   * Find the actual greeting that belongs to the ID or return a default greeting when none found.
   */
  public String findGreetingById(String id) {
    return switch (id.toLowerCase(Locale.ROOT)) {
      case "koen" -> "Hello, Koen!";
      case "pim" -> "Hello, Pim!";
      case "rockstar" -> "Hello, Rockstar!";
      default -> "Hello, World!";
    };
  }

}
