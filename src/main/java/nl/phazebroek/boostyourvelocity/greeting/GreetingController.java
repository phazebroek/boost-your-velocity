package nl.phazebroek.boostyourvelocity.greeting;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * Entrypoint of the /greetings resource.
 */
@RestController
@RequiredArgsConstructor
public class GreetingController {

  private final GreetingService greetingService;

  @GetMapping("/greetings")
  public ResponseEntity<String> getGreeting() {
    return getGreeting("world");
  }

  @GetMapping("/greetings/{id}")
  public ResponseEntity<String> getGreeting(@PathVariable("id") String id) {
    var greeting = greetingService.getGreeting(id);
    return ResponseEntity.ok(greeting);
  }

}
